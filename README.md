# SmartSchool


## O que é isso?
Essa aplicação foi desenvolvida para a seleção da AgendaKids

## Instalação
```sh       
  $ git clone https://bitbucket.org/alissonbruno/smart-school
  $ cd smart-school
  $ bundle install
  $ rake db:create
  $ rake db:migrate
  $ rake db:seed
```

## Utilizando aplicação
*email:* hogwarts@hogwarts.com
*password:* 123456