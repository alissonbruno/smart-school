class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_user!
  before_action :verify_complete_sign_up, unless: :devise_controller?

  layout :layout

  private 
    def verify_complete_sign_up
      redirect_to new_school_path if user_signed_in? && !current_user.completed
    end

    def layout
      devise_controller? ? "devise" : "application"
    end
end
