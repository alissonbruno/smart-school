module SetSchool
  extend ActiveSupport::Concern

  included do
    attr_reader :school
    before_action :set_school
  end

  def set_school
    @school = current_user.school if user_signed_in?
  end
end