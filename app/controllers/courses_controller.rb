class CoursesController < ApplicationController
  include SetSchool
  before_action :set_unit
  before_action :set_course, only: [:show, :edit, :update, :destroy]

  def index
    @courses = @unit.courses
  end

  def show
  end

  def new
    @course = @unit.courses.build
  end

  def edit
  end

  def create
    @course = @unit.courses.build(course_params)

    if @course.save
      redirect_to unit_courses_path(@unit), notice: 'Curso foi criado com sucesso.'
    else
      render :new
    end
  end

  def update
    if @course.update(course_params)
      redirect_to unit_courses_path(@unit), notice: 'Curso foi atualizado com sucesso.'
    else
      render :edit
    end
  end

  def destroy
    @course.destroy
    redirect_to unit_courses_url(@unit), notice: 'Curso foi removido com sucesso.'
  end

  private
    def set_unit
      @unit = school.units.find(params[:unit_id])
    end
    
    def set_course
      @course = @unit.courses.find(params[:id])
    end

    def course_params
      params.require(:course).permit(:name, :description, :teacher_id)
    end
end
