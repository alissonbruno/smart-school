class SchoolsController < ApplicationController
  skip_before_action :verify_complete_sign_up
  def new
    @school = current_user.school || current_user.build_school
  end

  def update
    @school = current_user.school
    if @school.update(school_params)
      redirect_to root_path, notice: 'Escola foi criada com sucesso.'
    else
      render :new
    end
  end

  def create 
    @school = current_user.build_school(school_params)
    if @school.save
      current_user.completed!
      redirect_to root_path, notice: 'Escola foi atualizada com sucesso.'
    else
      render :new
    end
  end

  private 
    def school_params
      params.require(:school).permit(:name, :phone, :email)
    end
end
