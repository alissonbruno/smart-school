class StudentsController < ApplicationController
  include SetSchool
  before_action :set_unit
  before_action :set_student, only: [:show, :edit, :update, :destroy]

  def index
    @students = @unit.students.page(params[:page]).per(10)
  end

  def show
  end

  def new
    @student = @unit.students.build
  end

  def edit
  end

  def create
    @student = @unit.students.build(student_params)
    if @student.save
      redirect_to unit_students_path(@unit), notice: 'Estudante foi criado com sucesso.'
    else
      render :new
    end
  end

  def update
    if @student.update(student_params)
      redirect_to unit_students_path(@unit), notice: 'Estudande foi atualizado com sucesso.'
    else
      render :edit
    end
  end

  def destroy
    @student.destroy
    redirect_to unit_students_url(@unit), notice: 'Estudante removido com sucesso.'
  end

  private
    def set_unit
      @unit = school.units.find(params[:unit_id])
    end

    def set_student
      @student = @unit.students.find(params[:id])
    end

    def student_params
      params.require(:student).permit(
        :name, 
        :registration, 
        :birthday, 
        course_ids: [], 
        legal_guardians_attributes: [:id, :name, :phone, :email, :_destroy]
      )
    end
end
