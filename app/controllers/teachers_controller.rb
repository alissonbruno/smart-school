class TeachersController < ApplicationController
  include SetSchool
  before_action :set_unit
  before_action :set_teacher, only: [:show, :edit, :update, :destroy]

  def index
    @teachers = @unit.teachers.page(params[:page]).per(10)
  end

  def show
  end

  def new
    @teacher = @unit.teachers.build
  end

  def edit
  end

  def create
    @teacher = @unit.teachers.build(teacher_params)

    if @teacher.save
      redirect_to unit_teachers_path(@unit), notice: "Professor foi criado com sucesso."
    else
      render :new
    end
  end

  def update
    if @teacher.update(teacher_params)
      redirect_to unit_teachers_path(@unit), notice: "Professor foi atualizado com sucesso."
    else
      render :edit
    end
  end

  def destroy
    begin
      @teacher.destroy
      redirect_to unit_teachers_url(@unit), notice: "O Professor foi removido com sucesso"
    rescue ActiveRecord::InvalidForeignKey
      redirect_to unit_teachers_url(@unit), notice: "Não foi possível remover o professor, o mesmo se encontra vinculado com um curso."
    end
    
  end

  private
    def set_unit
      @unit = school.units.find(params[:unit_id])
    end

    def set_teacher
      @teacher = @unit.teachers.find(params[:id])
    end

    def teacher_params
      params.require(:teacher).permit(:name, :email, :phone)
    end
end
