class UnitsController < ApplicationController
  include SetSchool
  before_action :set_unit, only: [:edit, :update, :destroy]

  def index
    @units = school.units
  end

  def new
    @unit = school.units.build
  end

  def edit
  end

  def create
    @unit = school.units.build(unit_params)

    if @unit.save
      redirect_to units_path, notice: "Unidade foi criada com sucesso."
    else
      render :new
    end
  end

  def update
    if @unit.update(unit_params)
      redirect_to units_path, notice: 'Unidade foi atualizada com sucesso.'
    else
      render :edit
    end
  end

  def destroy
    @unit.destroy
    redirect_to units_url, notice: 'Unidade foi removida com sucesso.'
  end

  private
    def set_unit
      @unit = school.units.find(params[:id])
    end

    def unit_params
      params.require(:unit).permit(:name, :phone, :email, :address)
    end
end
