class Course < ApplicationRecord
  belongs_to :unit, counter_cache: true
  belongs_to :teacher

  validates :unit, presence: true
  validates :teacher, presence: true
  validates :name, presence: true
  validates :description, presence: true, length: {minimum: 100}
  
end
