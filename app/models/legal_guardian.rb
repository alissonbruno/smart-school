class LegalGuardian < ApplicationRecord
  belongs_to :student, inverse_of: :legal_guardians, counter_cache: true

  validates :name, presence: true
  validates :email, presence: true, format: {with: Devise::email_regexp}
  validates :phone, presence: true
end
