class School < ApplicationRecord
  has_many :units

  validates :name, presence: true
  validates :email, presence: true, format: {with: Devise::email_regexp}
  validates :phone, presence: true
end
