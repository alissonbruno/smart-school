class Student < ApplicationRecord
  belongs_to :unit, counter_cache: true
  has_many :legal_guardians, inverse_of: :student, dependent: :destroy
  has_and_belongs_to_many :courses

  accepts_nested_attributes_for :legal_guardians, allow_destroy: true
  
  validates :unit, presence: true
  validates :name, presence: true
  validates :registration, presence: true, uniqueness: true
  validates :birthday, presence: true
  validates :legal_guardians , length: { minimum: 1 }
end
