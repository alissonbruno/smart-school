class Teacher < ApplicationRecord
  belongs_to :unit, counter_cache: true

  validates :unit, presence: true
  validates :name, presence: true
  validates :email, presence: true, format: { with: Devise::email_regexp }
  validates :phone, presence: true
end
