class Unit < ApplicationRecord
  belongs_to :school
  has_many :teachers
  has_many :students
  has_many :courses

  validates :school, presence: true
  validates :name, presence: true
  validates :email, presence: true, format: {with: Devise::email_regexp}
  validates :phone, presence: true
end
