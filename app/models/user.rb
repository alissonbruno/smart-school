class User < ApplicationRecord
  has_one :school
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  def completed!
    update_column(:completed, true)
  end
end
