Rails.application.routes.draw do
  devise_for :users
  resource :school, only: [:new, :create, :update], path_names: {new: ''}
  root to: 'home#index'

  resources :units, except: [:show] do
    resources :teachers
    resources :students
    resources :courses
  end
end
