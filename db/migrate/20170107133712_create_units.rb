class CreateUnits < ActiveRecord::Migration[5.0]
  def change
    create_table :units do |t|
      t.string :name
      t.string :phone
      t.string :email
      t.references :school, foreign_key: true
      t.string :address

      t.timestamps
    end
  end
end
