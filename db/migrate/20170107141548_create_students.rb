class CreateStudents < ActiveRecord::Migration[5.0]
  def change
    create_table :students do |t|
      t.string :name
      t.string :registration
      t.date :birthday
      t.references :unit, foreign_key: true

      t.timestamps
    end
  end
end
