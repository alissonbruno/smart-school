class CreateLegalGuardians < ActiveRecord::Migration[5.0]
  def change
    create_table :legal_guardians do |t|
      t.string :name
      t.string :phone
      t.string :email
      t.references :student, foreign_key: true

      t.timestamps
    end
  end
end
