class AddUserToSchools < ActiveRecord::Migration[5.0]
  def change
    add_reference :schools, :user, foreign_key: true
  end
end
