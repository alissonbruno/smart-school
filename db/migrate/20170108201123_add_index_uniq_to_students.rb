class AddIndexUniqToStudents < ActiveRecord::Migration[5.0]
  def change
    add_index :students, :registration, unique: true
  end
end
