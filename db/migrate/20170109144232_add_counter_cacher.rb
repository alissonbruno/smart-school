class AddCounterCacher < ActiveRecord::Migration[5.0]
  def change
    add_column :units, :students_count, :integer, default: 0
    add_column :units, :teachers_count, :integer, default: 0
    add_column :units, :courses_count, :integer, default: 0
  end
end
