class AddCounterCacheToStudents < ActiveRecord::Migration[5.0]
  def change
    add_column :students, :legal_guardians_count, :integer, default: 0
  end
end
