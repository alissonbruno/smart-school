User.transaction do 
  user = User.create!(
    email: "hogwarts@hogwarts.com",
    password: "123456",
    completed: true
  )

  school = user.create_school!(
    name: "Escola de Magia e Bruxaria de Hogwarts",
    email: "hogwarts@hogwarts.com",
    phone: "(85) 98585-8581"
  )

  school.units.create!({ name: "Grifinória (Gryffindor)", email: "grifinoria@hogwarts.com", phone: "(85) 98585-8582" })
  school.units.create!({ name: "Corvinal (Ravenclaw)", email: "corvinal@hogwarts.com", phone: "(85) 98585-8583" })
  school.units.create!({ name: "Sonserina (Slytherin)", email: "sonserina@hogwarts.com", phone: "(85) 98585-8584" })
  school.units.create!({ name: "Lufa-Lufa (Hufflepuff)", email: "lufa-lufa@hogwarts.com", phone: "(85) 98585-8585" })

  school.units.each do |unit| 
    rand(1..20).times do
      unit.teachers.create!(
        avatar: Faker::Avatar.image,
        name: Faker::Name.name, 
        phone: Faker::PhoneNumber.phone_number, 
        email: Faker::Internet.safe_email
      )
    end

    rand(1..60).times do
      unit.students.create!(
        name: Faker::Name.name,
        registration: Faker::Number.number(10),
        birthday: Faker::Date.between(15.days.ago, Date.today),
        legal_guardians_attributes: [{
          name: Faker::Name.name,
          phone: Faker::PhoneNumber.phone_number,
          email: Faker::Internet.email
        }]
      )
    end

    unit.teachers.each do |teacher|
      unit.courses.create!(
        name:  Faker::Educator.course, 
        description: Faker::Lorem.paragraphs(2),
        teacher: teacher
      )
    end

    unit.students.each do |student|
      student.courses << unit.courses.sample(rand(1..5))  
    end
  end
end