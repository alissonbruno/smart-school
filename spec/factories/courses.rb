FactoryGirl.define do
  factory :course, class: Course do
    association :unit, factory: :unit 
    association :teacher, factory: :teacher
    name  Faker::Educator.course
    description Faker::Lorem.paragraphs(2)
  end
end