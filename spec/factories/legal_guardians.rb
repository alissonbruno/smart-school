FactoryGirl.define do
  factory :legal_guardian, class: LegalGuardian do
    name Faker::Name.name
    phone Faker::PhoneNumber.phone_number
    email Faker::Internet.email
    student
  end
end