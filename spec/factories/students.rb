FactoryGirl.define do
  factory :guardian, class: LegalGuardian do
    name Faker::Educator.course
    phone Faker::PhoneNumber.phone_number
    email Faker::Internet.email
  end

  factory :student, class: Student do
    association :unit, factory: :unit
    name  Faker::Name.name
    registration Faker::Number.number(10)
    birthday Faker::Date.between(15.days.ago, Date.today)

    factory :student_with_guardians do
      after(:build) do |student|
        student.legal_guardians << create(:guardian, student:student)
      end
    end
  end
end