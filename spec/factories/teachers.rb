FactoryGirl.define do
  factory :teacher, class: Teacher do
    association :unit, factory: :unit 
    name  Faker::Name.name
    phone Faker::PhoneNumber.phone_number
    email Faker::Internet.email
  end
end