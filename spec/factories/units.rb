FactoryGirl.define do
  factory :unit, class: Unit do
    association :school, factory: :school 
    name  Faker::Name.name
    phone Faker::PhoneNumber.phone_number
    email Faker::Internet.email
    address Faker::Address.street_address
  end
end