FactoryGirl.define do
  factory :user, class: User do
    association :school, factory: :school
    email "user@example.com"
    password "12345678"
    password_confirmation "12345678"
    completed true
  end
end