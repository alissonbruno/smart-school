require 'rails_helper'

describe "School Page", type: :feature do
  describe "GET /" do
    let(:login_page) { LoginPage.new }
    let(:user) { create(:user, completed: false) } 
    before(:each) do 
      login_page.load
      @school_page = login_page.log_in(user.email, "12345678")
    end

    it "shows school's form after login if user doesn't have school" do
      expect(@school_page).to have_content("Escola")
      expect(@school_page).to have_css("input[type='submit']")
    end

    it "shows the home page after register school" do
      user.completed!
      home_page = @school_page.register(user.school)
      expect(home_page).to have_content("Escola foi criada com sucesso.")
    end
  end
end