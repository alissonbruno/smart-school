require 'rails_helper'

describe "Unit", type: :feature do
  describe "features" do
    let(:unit_page) { UnitPage.new }
    let(:user) { create(:user) }
    before(:each) do 
      login_as(user)
      unit_page.load
    end

    context "navigation" do
      it "navigates to /units" do
        expect(unit_page).to have_content("Unidades")
      end

      it "navigates to /units/new" do 
        new_unit_page = unit_page.navigate_to_new_unit
        expect(new_unit_page).to have_content("Nova unidade")
      end

      it "navigates to edit page" do
        unit = user.school.units.create(attributes_for(:unit))
        unit_page.navigate_to_edit_unit(unit.id)
        expect(page).to have_content("Edição unidade")
      end
    end

    context "validations" do
      let(:new_unit_page) do
        unit_page.navigate_to_new_unit
      end
      it "should register a new unit" do 
        unit = build(:unit)
        new_unit_page.register_unit(unit)
        expect(new_unit_page).to have_content(unit.name)
      end

      it "should show a error message" do
        unit = build(:unit, name: nil)
        new_unit_page.register_unit(unit)
        expect(new_unit_page).to have_content("Name can't be blank")
      end

      it "should not have a removed unit on /units page" do
        unit = user.school.units.create(attributes_for(:unit))
        unit_page.load
        expect(unit_page).to have_content(unit.name)
        unit.destroy
        unit_page.load
        expect(unit_page).to have_no_content(unit.name)
      end
    end
  end
end