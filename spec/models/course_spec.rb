require 'rails_helper'

RSpec.describe Course, type: :model do
  context "validations" do
    context "when not valid" do
      it "requires to belong to a unit" do 
        course = build(:course, unit: nil)
        expect(course).to have_at_least(1).error_on(:unit)
      end

      it "requires to have a teacher" do 
        course = build(:course, teacher: nil)
        expect(course).to have_at_least(1).error_on(:teacher)
      end

      it "requires name to be set" do
        course = build(:course, name: nil)
        expect(course).to have(1).error_on(:name)
      end

      it "requires description to be set" do
        course = build(:course, description: nil)
        expect(course).to have_at_least(1).error_on(:description)
      end

      it "requires description has at least 100 characters" do
        course = build(:course, description: "Descriçao curso menos de 100 palavras" ) 
        expect(course).to have_at_least(1).error_on(:description)
      end
    end

    context 'when valid' do
      it 'should create new course' do 
        course = create(:course)
        expect(course).to have(0).errors
      end
    end
  end
end
