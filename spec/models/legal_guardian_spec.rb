require 'rails_helper'

RSpec.describe LegalGuardian, type: :model do
  context "validations" do
    let(:student) { build(:student_with_guardians) }

    context "when not valid" do
      it "requires name to be set" do
        legal_guardian = build(:legal_guardian, name: nil, student: student)
        expect(legal_guardian).to have(1).error_on(:name)
      end

      it "requires email to be set" do
        legal_guardian = build(:legal_guardian, email: nil, student: student)
        expect(legal_guardian).to have_at_least(1).error_on(:email)
      end
      
      it "requires a valid email to be set" do
        legal_guardian = build(:legal_guardian, email: "invalidemail", student: student)
        expect(legal_guardian).to have(1).error_on(:email)
      end

      it "requires phone to be set" do
        legal_guardian = build(:legal_guardian, phone: nil, student: student)
        expect(legal_guardian).to have(1).error_on(:phone)
      end
    end

    context 'when valid' do
      it 'should create new legal_guardian' do 
        legal_guardian = create(:legal_guardian, student: student)
        expect(legal_guardian).to have(0).errors
      end
    end
  end
end
