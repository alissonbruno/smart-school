require 'rails_helper'

RSpec.describe School, type: :model do
  context "validations" do
    context "when not valid" do
      it "requires name to be set" do
        school = build(:school, name: nil)
        expect(school).to have(1).error_on(:name)
      end

      it "requires email to be set" do
        school = build(:school, email: nil)
        expect(school).to have_at_least(1).error_on(:email)
      end

      it "requires a valid email to be set" do
        school = build(:school, email: "invalidemail")
        expect(school).to have(1).error_on(:email)
      end

      it "requires phone to be set" do
        school = build(:school, phone: nil)
        expect(school).to have(1).error_on(:phone)
      end
    end

    context 'when valid' do
      it 'should create new school' do 
        school = create(:school)
        expect(school).to have(0).errors
      end
    end
  end
end
