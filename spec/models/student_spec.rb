require 'rails_helper'

RSpec.describe Student, type: :model do
  context "validations" do
    context "when not valid" do
      it "requires at least one legal guardian" do 
        student = build(:student)
        expect(student).to have(1).error_on(:legal_guardians)
      end

      it "requires to belong to a unit" do 
        student = build(:student, unit: nil)
        expect(student).to have_at_least(1).error_on(:unit)
      end

      it "requires name to be set" do
        student = build(:student, name: nil)
        expect(student).to have(1).error_on(:name)
      end

      it "requires registration to be set" do
        student = build(:student, registration: nil)
        expect(student).to have(1).error_on(:registration)
      end

      it "requires registration to be unique" do
        create(:student_with_guardians, registration: "101010")
        student = build(:student_with_guardians, registration: "101010")
        student.valid?
        expect(student).to have(1).error_on(:registration)
      end

      it "requires birthday to be set" do
        student = build(:student, birthday: nil)
        expect(student).to have(1).error_on(:birthday)
      end
    end

    context 'when valid' do
      it 'should create new student' do 
        student = build(:student_with_guardians)
        student.save
        expect(student).to have(0).errors
      end
    end
  end
end
