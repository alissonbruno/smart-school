require 'rails_helper'

RSpec.describe Teacher, type: :model do
  context "validations" do
    context "when not valid" do
      it "requires to belong to a unit" do 
        teacher = build(:teacher, unit: nil)
        expect(teacher).to have_at_least(1).error_on(:unit)
      end

      it "requires name to be set" do
        teacher = build(:teacher, name: nil)
        expect(teacher).to have(1).error_on(:name)
      end

      it "requires email to be set" do
        teacher = build(:teacher, email: nil)
        expect(teacher).to have_at_least(1).error_on(:email)
      end

       it "requires a valid email to be set" do
        teacher = build(:teacher, email: "invalidemail")
        expect(teacher).to have(1).error_on(:email)
      end

      it "requires phone to be unique" do
        teacher = build(:teacher, phone: nil)
        expect(teacher).to have(1).error_on(:phone)
      end
    end

    context 'when valid' do
      it 'should create new teacher' do 
        teacher = create(:teacher)
        expect(teacher).to have(0).errors
      end
    end
  end
end
