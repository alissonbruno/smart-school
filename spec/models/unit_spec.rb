require 'rails_helper'

RSpec.describe Unit, type: :model do
  context "validations" do
    context "when not valid" do
      it "requires to belong to school" do 
        unit = build(:unit, school: nil)
        expect(unit).to have_at_least(1).error_on(:school)
      end

      it "requires name to be set" do
        unit = build(:unit, name: nil)
        expect(unit).to have(1).error_on(:name)
      end

      it "requires email to be set" do
        unit = build(:unit, email: nil)
        expect(unit).to have_at_least(1).error_on(:email)
      end

      it "requires a valid email to be set" do
        unit = build(:unit, email: "invalidemail")
        expect(unit).to have(1).error_on(:email)
      end

      it "requires phone to be set" do
        unit = build(:unit, phone: nil)
        expect(unit).to have(1).error_on(:phone)
      end
    end

    context 'when valid' do
      it 'should create new unit' do 
        unit = create(:unit)
        expect(unit).to have(0).errors
      end
    end
  end
end
