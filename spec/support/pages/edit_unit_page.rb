class EditUnitPage < SitePrism::Page
  set_url "/units{/id}/edit"
end