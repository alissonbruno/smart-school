class HomePage < SitePrism::Page
  set_url "/"

  element :units_link, "#units_link"

  def units_page
    units_link.click
    UnitPage.new
  end
end