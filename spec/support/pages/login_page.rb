class LoginPage < SitePrism::Page
  set_url "/"

  element :email_field,    "#user_email"
  element :password_field, "#user_password"

  def log_in(username, password)
    email_field.set(username)
    password_field.set(password)
    click_on('Entrar')
    SchoolPage.new
  end
end