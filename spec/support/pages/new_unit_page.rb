class NewUnitPage < SitePrism::Page
  set_url "/units/new"

  element :unit_name, "#unit_name"
  element :unit_phone, "#unit_phone"
  element :unit_email, "#unit_email"
  element :unit_address, "#unit_address"
  element :form_submit, "input[type='submit']"

  def register_unit(unit)
    unit_name.set unit.name
    unit_phone.set unit.phone
    unit_email.set unit.email
    unit_address.set unit.address
    form_submit.click
  end
end