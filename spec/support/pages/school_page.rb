class SchoolPage < SitePrism::Page
  set_url "/school"

  element :name_field, "#school_name"
  element :phone_field, "#school_phone"
  element :email_field, "#school_email"
  element :form_button, "input[type='submit']"

  def register(school)
    name_field.set school.name
    phone_field.set school.phone
    email_field.set school.email
    form_button.click
    HomePage.new
  end
end