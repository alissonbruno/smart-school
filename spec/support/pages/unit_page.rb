class UnitPage < SitePrism::Page
  set_url "/units"
  element :new_unit_link, '#new_unit_link'

  def navigate_to_new_unit
    new_unit_link.click
    NewUnitPage.new
  end

  def navigate_to_edit_unit(id)
    EditUnitPage.new.load(id: id)
  end
end